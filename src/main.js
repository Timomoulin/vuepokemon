import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap/dist/js/bootstrap.bundle"
import axiosApi from 'axios'

let axios=axiosApi.create({
    baseURL:"https://pokeapi.co/api/v2"
});
window.axios=axios;

createApp(App).mount('#app')
